import React, { Component, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';


const Detail = ({ route, navigation }) => {
  const { item } = route.params;

  const [user, setUser] = useState({
    firstName: item.firstName,
    lastname: item.lastname,
    age: item.age,
    photo: item.photo,
  });

  const onChangeFirstName = (value) => {
    setUser({ ...user, firstName: value });
  };

  const onChangeLastName = (value) => {
    setUser({ ...user, lastname: value });
  };

  const onChangeAge = (value) => {
    setUser({ ...user, age: value });
  };

  const onChangePhoto = (value) => {
    setUser({ ...user, photo: value });
  };

  const updateData = () => {
    var myHeaders = new Headers();

    myHeaders.append('Content-Type', 'application/json');

    fetch('https://simple-contact-crud.herokuapp.com/contact/'+item.id, {
      method: 'PATCH',
      headers: myHeaders,
      body: JSON.stringify({
        firstName: user.firstName,
        lastname: user.lastname,
        age: user.age,
        photo: user.photo,
      }),
    })
      .then((response) => {
        response.text();
        navigation.push('Get')
      })
      .then((result) => console.log(result))
      .catch((error) => console.log(error));
  };

  const deleteData = () => {
    var myHeaders = new Headers();

    myHeaders.append('Content-Type', 'application/json');

    fetch('https://simple-contact-crud.herokuapp.com/contact/'+item.id, {
      method: 'DELETE',
      headers: myHeaders,
      body: JSON.stringify({
        firstName: user.firstName,
        lastname: user.lastname,
        age: user.age,
        photo: user.photo,
      }),
    })
      .then((response) => {
        response.text();
        navigation.push('Get')
      })
      .then((result) => console.log(result))
      .catch((error) => console.log(error));
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'First Name'}
        onChangeText={(value) => onChangeFirstName(value)}
        style={styles.input}
        value={user.firstName}
      />
      <TextInput
        placeholder={'Last Name'}
        onChangeText={(value) => onChangeLastName(value)}
        style={styles.input}
        value={user.lastname}
      />
      <TextInput
        placeholder={'Age'}
        onChangeText={(value) => onChangeAge(value)}
        style={styles.input}
        value={user.age}
      />
      <TextInput
        placeholder={'Photo'}
        onChangeText={(value) => onChangePhoto(value)}
        style={styles.input}
        value={user.photo}
      />

      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={updateData}>
          <View style={{ backgroundColor: 'blue', padding: 10 }}>
            <Text style={{ color: 'white', textAlign: 'center' }}>Update</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={deleteData}>
          <View style={{ backgroundColor: 'red', padding: 10 }}>
            <Text style={{ color: 'white', textAlign: 'center' }}>Hapus</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    backgroundColor: '#fff',
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginVertical: 5,
  },
});

export default Detail;
